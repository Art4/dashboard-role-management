<?php
/*
Plugin Name: Dashboard Role Management
Plugin URI: http://www.wlabs.de
Description: Dieses Plugin l�sst nur noch bestimmte Rollen ins Dashboard. Alle anderen werden zu einer anderen Seite weitergeleitet, die selbst bestimmt werden kann.
Author: Artur Weigandt
Version: 0.5
Author URI: http://www.wlabs.de
*/

//relative Plugin-Paths
define('DRM_PLUGIN_FILEPATH', plugin_basename( __FILE__ )); // dashboard-role-management/dashboard-role-management.php
define('DRM_PLUGIN_PATH', plugin_basename(dirname( __FILE__ ))); // dashboard-role-management
define('DRM_PLUGIN_FILENAME', str_replace(DRM_PLUGIN_PATH.'/', '', plugin_basename(__FILE__))); // dashboard-role-management.php

//Plugin-URL
define('DRM_PLUGIN_URL', WP_PLUGIN_URL . '/' . DRM_PLUGIN_PATH); //http://www.example.com/wp-content/plugins/dashboard-role-management

//Im Admin-Bereich abfangen
add_action('admin_init', 'drm_redirect', 1);

//Admin-Menue aktivieren
add_action('admin_menu', 'drm_add_pages');

//Installations-Routine
register_activation_hook(DRM_PLUGIN_FILENAME, 'drm_install');
		
//Deinstallations-Routine
register_deactivation_hook(DRM_PLUGIN_FILENAME, 'drm_uninstall');


function drm_redirect(){
	global $current_user;
	$options = get_option('drm_options');
	
	$role = $current_user->roles[0];
	
	if($options[$role]['redirect'] === true)
	{
		$url = (empty($options[$role]['url'])) ? get_option('home') : $options[$role]['url'];
		$url = str_replace('[home]', get_option('home'), $url);
		
		wp_redirect($url);
	}
}

function drm_install()
{
	//Standard-Einstellungen
	$default_options = array(
		'editor' => array('redirect' => false, 'url' => ''),
		'author' => array('redirect' => false, 'url' => ''),
		'contributer' => array('redirect' => false, 'url' => ''),
		'subscriber' => array('redirect' => false, 'url' => ''),
	);
	
	add_option('drm_options', $default_options);
}

function drm_uninstall()
{
	delete_option('drm_options');
}

function drm_add_pages()
{
	$page = add_options_page('Dashboard Role Management Einstellungen', 'Dashboard Role Management', 8, DRM_PLUGIN_FILEPATH, 'drm_options_page');
	
	add_action('admin_print_scripts-' . $page, 'drm_load_admin_head');
}

function drm_load_admin_head()
{
	//wp_enqueue_script('loadjs', WLEE_PLUGIN_URL . 'js/wlee_admin.js');
	
	echo "<link rel='stylesheet' href='" . DRM_PLUGIN_URL . "/style.css' type='text/css' />\n";
}

//Options-Page im Admin-Panel
function drm_options_page()
{
	// Wenn das Formular abgesendet wurde
	if (isset($_POST['submit'])) 
	{
		check_admin_referer('dashboard-role-management');
		
		//Formular auslesen
		
		//editor
		$editor_redirect = (!isset($_POST['drm_editor_redirect'])) ? false: true;
		$editor_url = (isset($_POST['drm_editor_url'])) ? $_POST['drm_editor_url'] : '';
		
		//author
		$author_redirect = (!isset($_POST['drm_author_redirect'])) ? false: true;
		$author_url = (isset($_POST['drm_author_url'])) ? $_POST['drm_author_url'] : '';
		
		//contributer
		$contributer_redirect = (!isset($_POST['drm_contributer_redirect'])) ? false: true;
		$contributer_url = (isset($_POST['drm_contributer_url'])) ? $_POST['drm_contributer_url'] : '';
		
		//subscriber
		$subscriber_redirect = (!isset($_POST['drm_subscriber_redirect'])) ? false: true;
		$subscriber_url = (isset($_POST['drm_subscriber_url'])) ? $_POST['drm_subscriber_url'] : '';
		
		//Einstellungen zusammenstellen
		$options = array(
			'editor' => array('redirect' => $editor_redirect, 'url' => $editor_url),
			'author' => array('redirect' => $author_redirect, 'url' => $author_url),
			'contributer' => array('redirect' => $contributer_redirect, 'url' => $contributer_url),
			'subscriber' => array('redirect' => $subscriber_redirect, 'url' => $subscriber_url),
		);
		
		update_option('drm_options', $options);
		
		$msg_status = __('Einstellungen gespeichert', 'dashboard-role-management');
		
		// Show message
		echo '<div id="message" class="updated fade"><p>' . $msg_status . '</p></div>';
	}
	
	// Einstellungen neu aus der db holen
	$options = get_option('drm_options');
	
	$editor_redirect = ($options['editor']['redirect']) ? ' checked="checked"' : '';
	$editor_url = $options['editor']['url'];
	
	$author_redirect = ($options['author']['redirect']) ? ' checked="checked"' : '';
	$author_url = $options['author']['url'];
	
	$contributer_redirect = ($options['contributer']['redirect']) ? ' checked="checked"' : '';
	$contributer_url = $options['contributer']['url'];
	
	$subscriber_redirect = ($options['subscriber']['redirect']) ? ' checked="checked"' : '';
	$subscriber_url = $options['subscriber']['url'];
	
	$wpnonce = wp_create_nonce('dashboard-role-management');
	$action_url = $_SERVER['REQUEST_URI'];
?>
<div class="wrap">
	<h2>Dashboard Role Management</h2>
	<form name="drm_form" action="<?php echo $action_url; ?>" method="post">
	
	<h3><?php _e('W&auml;hle aus, wer nicht ins Dashboard darf und wohin umgeleitet werden soll', 'dashboard-role-management'); ?></h3>
	<?php printf(__('Wird keine URL angegeben, wird nach "%s" weitergeleitet.', 'dashboard-role-management'), '<span class="info">' . get_option('home') . '</span>'); ?>
		<table class="form-table">
			<tr valign="top">
				<th><input id="editor_redirect" type="checkbox" name="drm_admin_redirect" disabled="disabled" /> <?php _e('Admins umleiten nach', 'dashboard-role-management'); ?></th>
				<td><input id="editor_url" type="text" size="60" name="drm_admin_url" value="<?php _e('Aussperrung von Administratoren nicht m&ouml;glich', 'dashboard-role-management'); ?>" readonly="readonly" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<input id="editor_redirect" type="checkbox" name="drm_editor_redirect"<?php echo $editor_redirect; ?> /> <label for="editor_redirect"><?php _e('Redakteure umleiten nach', 'dashboard-role-management'); ?></label>
				</th>
				<td><input id="editor_url" type="text" size="60" name="drm_editor_url" value="<?php echo $editor_url; ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<input id="author_redirect" type="checkbox" name="drm_author_redirect"<?php echo $author_redirect; ?> /> <label for="author_redirect"><?php _e('Autoren umleiten nach', 'dashboard-role-management'); ?></label>
				</th>
				<td><input id="author_url" type="text" size="60" name="drm_author_url" value="<?php echo $author_url; ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<input id="contributer_redirect" type="checkbox" name="drm_contributer_redirect"<?php echo $contributer_redirect; ?> /> <label for="contributer_redirect"><?php _e('Mitarbeiter umleiten nach', 'dashboard-role-management'); ?></label>
				</th>
				<td><input id="contributer_url" type="text" size="60" name="drm_contributer_url" value="<?php echo $contributer_url; ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<input id="subscriber_redirect" type="checkbox" name="drm_subscriber_redirect"<?php echo $subscriber_redirect; ?> /> <label for="subscriber_redirect"><?php _e('Abonnenten umleiten nach', 'dashboard-role-management'); ?></label>
				</th>
				<td><input id="subscriber_url" type="text" size="60" name="drm_subscriber_url" value="<?php echo $subscriber_url; ?>" /></td>
			</tr>
		</table>
		<p><?php printf(__('Du kannst <span class="info">[home]</span> als Platzhalter f&uuml;r "%s" verwenden.', 'dashboard-role-management'), '<span class="info">' . get_option('home') . '</span>'); ?></p>
		
		<p class="submit">
			<input type="hidden" name="submit" value="1" /> 
			<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo $wpnonce; ?>" />
			<input type="submit" class="button-primary" name="Submit" value="<?php _e('Speichern', 'dashboard-role-management'); ?>" />
		</p>
		</form>
	</div>
	
	<?php
	}

?>