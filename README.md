dashboard-role-management
=========================

Dashboard Role Management ist ein Wordpress-Plugin, mit dem bestimmte WP-Rollen vom Dashboard ausgeschlossen werden können und stattdessen zu einer selbst bestimmten URL weitergeleitet werden.

siehe auch: http://www.wlabs.de/plugins/dashboard-role-management/
